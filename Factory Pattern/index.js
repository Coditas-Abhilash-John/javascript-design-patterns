function Developer(name)
{
  this.name = name
  this.type = "Developer"
}

function Tester(name)
{
  this.name = name
  this.type = "Tester"
}

function EmployeeFactory()
{
  this.create = (name, type) => {
    switch(type)
    {
      case 1:
        return new Developer(name)
      case 2:
        return new Tester(name)
    }
  }
}

function say()
{
  console.log("Hi, I am " + this.name + " and I am a " + this.type)
}

const employeeFactory = new EmployeeFactory()
const employees = []

employees.push(employeeFactory.create("Ram", 2))
employees.push(employeeFactory.create("Tam", 1))
employees.push(employeeFactory.create("Rahul", 1))
employees.push(employeeFactory.create("Nathan", 1))
employees.push(employeeFactory.create("Ninnan", 1))

employees.forEach( emp => {
  say.call(emp)
})